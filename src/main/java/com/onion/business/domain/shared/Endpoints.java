package com.onion.business.domain.shared;

public final class Endpoints {

    private Endpoints() { /*no-op*/ }

    public static final String API_URI = "/api";
}
