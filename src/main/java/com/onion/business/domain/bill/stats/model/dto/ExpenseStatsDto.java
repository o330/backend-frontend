package com.onion.business.domain.bill.stats.model.dto;

import com.onion.business.domain.bill.expense.model.Expense;
import com.onion.business.domain.categories.model.CategoryDto;

import java.math.BigDecimal;

public record ExpenseStatsDto(CategoryDto category, BigDecimal value) {
    public static ExpenseStatsDto of(Expense expense) {
        BigDecimal value = expense.getUnitCost().multiply(BigDecimal.valueOf(expense.getQuantity()));
        return new ExpenseStatsDto(CategoryDto.of(expense.getCategory()), value);
    }

    public ExpenseStatsDto add(ExpenseStatsDto expenseStatsDto) {
        return new ExpenseStatsDto(expenseStatsDto.category, value.add(expenseStatsDto.value));
    }
}
