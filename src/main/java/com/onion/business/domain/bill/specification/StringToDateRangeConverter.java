package com.onion.business.domain.bill.specification;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.vavr.control.Try;
import org.springframework.core.convert.converter.Converter;

public class StringToDateRangeConverter implements Converter<String, DateRange> {
    @Override
    public DateRange convert(String source) {
        ObjectMapper mapper = JsonMapper.builder()
                .addModule(new JavaTimeModule())
                .build();
        return Try.of(() -> mapper.readValue(source, DateRange.class)).get();
    }
}
