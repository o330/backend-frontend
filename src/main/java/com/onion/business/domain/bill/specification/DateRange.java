package com.onion.business.domain.bill.specification;

import java.time.LocalDateTime;

public record DateRange(LocalDateTime start, LocalDateTime end) {
}
