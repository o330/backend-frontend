package com.onion.business.domain.bill.control;

import com.onion.business.domain.bill.model.Bill;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface BillRepository extends JpaRepository<Bill, Long> {
    @Query("SELECT b from Bill b where b.userId =  :userId")
    List<Bill> findByUsername(@Param("userId") Long userId);

    @Query("SELECT b FROM Bill b WHERE b.userId =  :userId AND b.addedAt BETWEEN :startDate AND :endDate")
    List<Bill> findByUsernameAndAddedAtBetween(@Param("userId") Long userId,
                                               @Param("startDate") LocalDateTime startDate,
                                               @Param("endDate") LocalDateTime endDate);

    @Query("SELECT b FROM Bill b WHERE b.userId =  :userId AND b.addedAt BETWEEN :startDate AND :endDate")
    List<Bill> findByUsernameAndAddedAtBetween(@Param("userId") Long userId,
                                               @Param("startDate") LocalDateTime startDate,
                                               @Param("endDate") LocalDateTime endDate,
                                               Sort sort);
}
