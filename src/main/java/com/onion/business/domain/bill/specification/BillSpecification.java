package com.onion.business.domain.bill.specification;

import com.onion.business.domain.bill.expense.model.Expense;
import com.onion.business.domain.bill.model.Bill;
import com.onion.business.domain.shop.Shop;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public record BillSpecification(DateRange dateRange, Long shopId, Long categoryId, PriceRange priceRange) {
    public boolean satisfies(Bill bill) {
        return satisfiesShop(bill.getShop())
                && satisfiesCategory(bill)
                && satisfiesPriceRange(bill);
    }

    private boolean satisfiesShop(Shop shop) {
        return this.shopId == null || (shop != null && Objects.equals(shop.getId(), this.shopId));
    }

    private boolean satisfiesCategory(Bill bill) {
        List<Expense> expense = bill.getExpenses();
        return this.categoryId == null || expense
                .stream()
                .map(Expense::getCategory)
                .anyMatch((category) -> {
                    if (categoryId < 0) {
                        return category == null;
                    } else {
                        return category != null && categoryId.equals(category.getId());
                    }
                });
    }

    private boolean satisfiesPriceRange(Bill bill) {
        BigDecimal total = bill.getExpenses()
                .stream()
                .map((expense) -> expense.getUnitCost().multiply(BigDecimal.valueOf(expense.getQuantity())))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
        if (priceRange.from() == null && priceRange.to() == null) {
            return true;
        }
        if (priceRange.to() == null) {
            return priceRange.from().compareTo(total) <= 0;
        }
        BigDecimal from = priceRange.from() == null ? BigDecimal.ZERO : priceRange.from();
        PriceRange priceRange = new PriceRange(from, this.priceRange.to());
        return priceRange.isBetween(total);
    }
}
