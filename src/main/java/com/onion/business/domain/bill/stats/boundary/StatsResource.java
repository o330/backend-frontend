package com.onion.business.domain.bill.stats.boundary;

import com.onion.business.domain.bill.stats.control.StatsService;
import com.onion.business.domain.bill.stats.model.dto.ExpenseStatsDto;
import com.onion.business.domain.user.UsernamePasswordAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Collection;

@RestController
@RequestMapping("/stats")
public class StatsResource {

    private final StatsService statsService;

    @Autowired
    public StatsResource(StatsService statsService) {
        this.statsService = statsService;
    }

    @GetMapping("/total")
    public ResponseEntity<Collection<ExpenseStatsDto>> totalSpending(UsernamePasswordAuthentication principal) {
        Collection<ExpenseStatsDto> categoryToSpending = statsService.totalSpending(principal);
        return new ResponseEntity<>(categoryToSpending, HttpStatus.OK);
    }

    @GetMapping("/period")
    public ResponseEntity<Collection<ExpenseStatsDto>> monthSpending(UsernamePasswordAuthentication principal,
                                                                     @RequestParam(name = "from") LocalDateTime from,
                                                                     @RequestParam(name = "to") LocalDateTime to) {
        Collection<ExpenseStatsDto> categoryToSpending = statsService.spendingOverPeriodOfTime(principal, from, to);
        return new ResponseEntity<>(categoryToSpending, HttpStatus.OK);
    }
}
