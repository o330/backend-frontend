package com.onion.business.domain.bill.stats.control;

import com.onion.business.domain.bill.control.BillRepository;
import com.onion.business.domain.bill.model.Bill;
import com.onion.business.domain.bill.stats.model.dto.ExpenseStatsDto;
import com.onion.business.domain.bill.expense.model.Expense;
import com.onion.business.domain.user.User;
import com.onion.business.domain.user.UsernamePasswordAuthentication;
import com.onion.system.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Service
public class StatsService {
    BillRepository billRepository;

    @Autowired
    public StatsService(BillRepository billRepository) {
        this.billRepository = billRepository;
    }

    public Collection<ExpenseStatsDto> totalSpending(UsernamePasswordAuthentication principal) {
        User user = principal.getUser();
        List<Bill> bills = billRepository
                .findByUsername(user.getId());
        return sumUp(bills);
    }

    public Collection<ExpenseStatsDto> spendingOverPeriodOfTime(UsernamePasswordAuthentication principal,
                                                                LocalDateTime startDate,
                                                                LocalDateTime endDate) {
        User user = principal.getUser();
        LocalDateTime startOfDay = DateUtils.atStartOfDay(startDate);
        LocalDateTime endOfDay = DateUtils.atEndOfDay(endDate);
        List<Bill> bills = billRepository
                .findByUsernameAndAddedAtBetween(user.getId(), startOfDay, endOfDay);
        return sumUp(bills);
    }

    private Collection<ExpenseStatsDto> sumUp(List<Bill> bills) {
        BiConsumer<Bill, Consumer<Expense>> mapToExpense = (bill, consumer) -> {
            for (Expense expense : bill.getExpenses()) {
                consumer.accept(expense);
            }
        };
        return bills.stream()
                .mapMulti(mapToExpense)
                .collect(Collectors.toMap(Expense::getCategory, ExpenseStatsDto::of, ExpenseStatsDto::add))
                .values();
    }
}
