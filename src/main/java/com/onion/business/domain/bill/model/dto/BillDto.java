package com.onion.business.domain.bill.model.dto;

import com.onion.business.domain.bill.model.Bill;
import com.onion.business.domain.bill.expense.model.ExpenseDto;
import com.onion.business.domain.shop.Shop;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public record BillDto(Long id, LocalDateTime addedAt, Shop shop, String imageUri, List<ExpenseDto> expenses) {
    public static BillDto of(Bill bill) {
        List<ExpenseDto> expensesDto = bill
                .getExpenses()
                .stream()
                .map(ExpenseDto::of)
                .collect(Collectors.toList());
        return new BillDto(bill.getId(), bill.getAddedAt(), bill.getShop(), bill.getImageUri(), expensesDto);
    }
}

