package com.onion.business.domain.bill.specification;

import java.math.BigDecimal;

public record PriceRange(BigDecimal from, BigDecimal to) {
    public boolean isBetween(BigDecimal number) {
        return  from.compareTo(to) <= 0
                && from.compareTo(number) <= 0
                && to.compareTo(number) >= 0;
    }
}
