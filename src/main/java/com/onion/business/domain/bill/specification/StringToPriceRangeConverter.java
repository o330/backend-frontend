package com.onion.business.domain.bill.specification;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import io.vavr.control.Try;
import org.springframework.core.convert.converter.Converter;

public class StringToPriceRangeConverter implements Converter<String, PriceRange> {
    @Override
    public PriceRange convert(String source) {
        ObjectMapper mapper = JsonMapper.builder()
                .build();
        return Try.of(() -> mapper.readValue(source, PriceRange.class)).get();
    }
}
