package com.onion.business.domain.bill.boundary;

import com.onion.business.domain.bill.control.BillService;
import com.onion.business.domain.bill.expense.model.ExpenseDto;
import com.onion.business.domain.bill.model.dto.BillDto;
import com.onion.business.domain.bill.specification.BillSpecification;
import com.onion.business.domain.user.UsernamePasswordAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/bills/")
public class BillResource {
    private final BillService billService;

    @Autowired
    public BillResource(BillService billService) {
        this.billService = billService;
    }

    @GetMapping
    public List<BillDto> getAllBills(UsernamePasswordAuthentication principal, BillSpecification specification) {
        return billService.findBillsSatisfyingSpecification(principal, specification)
                .stream()
                .map(BillDto::of)
                .collect(Collectors.toList());
    }

    @PostMapping
    public BillDto persistBill(UsernamePasswordAuthentication principal, @RequestBody BillDto bill) {
        return billService.persistBill(principal, bill);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> deleteBill(@PathVariable Long id) {
        billService.deleteBill(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @PutMapping("/expense")
    public ExpenseDto updateExpense(@RequestBody ExpenseDto expense) {
        return billService.persistExpense(expense);
    }
}
