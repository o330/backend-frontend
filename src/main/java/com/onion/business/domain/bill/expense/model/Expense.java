package com.onion.business.domain.bill.expense.model;

import com.onion.business.domain.categories.model.Category;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class Expense {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String title;

    @ManyToOne(targetEntity = Category.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", insertable = false, updatable = false)
    private Category category;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(nullable = false)
    private BigDecimal unitCost;

    @Column(nullable = false)
    private Double quantity;

    @Column(nullable = false)
    private String currency;

    public static Expense of(ExpenseDto expenseDto) {
        return new ExpenseBuilder()
                .id(expenseDto.id())
                .title(expenseDto.title())
                .categoryId(expenseDto.category().id())
                .quantity(expenseDto.quantity())
                .unitCost(expenseDto.unitCost())
                .currency(expenseDto.currency())
                .build();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Expense expense = (Expense) o;
        return id.equals(expense.id) && title.equals(expense.title)
                && category.equals(expense.category)
                && unitCost.equals(expense.unitCost)
                && currency.equals(expense.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, category, unitCost, currency);
    }
}
