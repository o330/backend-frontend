package com.onion.business.domain.bill.control;

import com.onion.business.domain.bill.expense.ExpenseRepository;
import com.onion.business.domain.bill.expense.model.Expense;
import com.onion.business.domain.bill.expense.model.ExpenseDto;
import com.onion.business.domain.bill.model.Bill;
import com.onion.business.domain.bill.model.dto.BillDto;
import com.onion.business.domain.bill.specification.BillSpecification;
import com.onion.business.domain.bill.specification.DateRange;
import com.onion.business.domain.user.User;
import com.onion.business.domain.user.UsernamePasswordAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static com.onion.system.utils.DateUtils.atEndOfDay;
import static com.onion.system.utils.DateUtils.atStartOfDay;

@Service
public class BillService {

    private final BillRepository billRepository;
    private final ExpenseRepository expenseRepository;

    @Autowired
    public BillService(BillRepository billRepository, ExpenseRepository expenseRepository) {
        this.billRepository = billRepository;
        this.expenseRepository = expenseRepository;
    }

    public List<Bill> findBillsSatisfyingSpecification(UsernamePasswordAuthentication principal,
                                                       BillSpecification specification) {
        DateRange dateRange = specification.dateRange();
        return findBillsSatisfyingSpecification(principal,
                atStartOfDay(dateRange.start()), atEndOfDay(dateRange.end()))
                .stream()
                .filter(specification::satisfies)
                .toList();
    }

    public List<Bill> findBillsSatisfyingSpecification(UsernamePasswordAuthentication principal,
                                                       LocalDateTime from, LocalDateTime to) {
        User user = principal.getUser();
        return billRepository.findByUsernameAndAddedAtBetween(user.getId(),
                atStartOfDay(from), atEndOfDay(to),
                JpaSort.by(Sort.Direction.DESC, "addedAt"));
    }

    public BillDto persistBill(UsernamePasswordAuthentication principal, BillDto billDto) {
        Bill bill = Bill.of(billDto);
        User user = principal.getUser();
        bill.setUserId(user.getId());
        return BillDto.of(billRepository.save(bill));
    }

    public void deleteBill(Long id) {
        billRepository.deleteById(id);
    }

    public ExpenseDto persistExpense(ExpenseDto expenseDto) {
        Expense expense = expenseRepository.save(Expense.of(expenseDto));
        return ExpenseDto.of(expense);
    }
}
