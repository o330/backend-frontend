package com.onion.business.domain.bill.model;

import com.onion.business.domain.bill.model.dto.BillDto;
import com.onion.business.domain.bill.expense.model.Expense;
import com.onion.business.domain.shop.Shop;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class Bill {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private LocalDateTime addedAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shop_id")
    private Shop shop;

    @Column(nullable = false)
    private Long userId;

    @Column
    private String imageUri;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "bill_id")
    private List<Expense> expenses;

    public static Bill of(BillDto billDto) {
        List<Expense> expenses = billDto.expenses().stream().map(Expense::of).collect(Collectors.toList());
        return new BillBuilder()
                .id(billDto.id())
                .shop(billDto.shop())
                .addedAt(billDto.addedAt())
                .imageUri(billDto.imageUri())
                .expenses(expenses)
                .build();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bill bill = (Bill) o;
        return id.equals(bill.id) && addedAt.equals(bill.addedAt) && Objects.equals(shop, bill.shop) && userId.equals(bill.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, addedAt, shop, userId);
    }
}
