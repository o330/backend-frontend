package com.onion.business.domain.bill.expense.model;

import com.onion.business.domain.categories.model.CategoryDto;

import java.math.BigDecimal;

public record ExpenseDto(Long id, CategoryDto category, String title, Double quantity, BigDecimal unitCost, String currency) {
    public static ExpenseDto of(Expense expense) {
        return new ExpenseDto(
                expense.getId(),
                CategoryDto.of(expense.getCategory()),
                expense.getTitle(),
                expense.getQuantity(),
                expense.getUnitCost(),
                expense.getCurrency());
    }
}
