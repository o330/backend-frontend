package com.onion.business.domain.user.boundary;

import com.onion.business.domain.auth.control.AuthenticationService;
import com.onion.business.domain.shared.Endpoints;
import com.onion.business.domain.user.User;
import com.onion.business.domain.user.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(UserResource.USER_URI)
@RequiredArgsConstructor
public class UserResource {
    static final String USER_URI = Endpoints.API_URI + "/user";
    private final AuthenticationService authenticationService;

    @PostMapping("/add")
    public ResponseEntity<User> addUser(@RequestBody UserDto user) {
        User createdUser = authenticationService.createUser(user);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(createdUser);
    }
}
