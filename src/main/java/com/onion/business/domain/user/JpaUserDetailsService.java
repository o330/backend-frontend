package com.onion.business.domain.user;

import com.onion.business.domain.user.authorities.Authority;
import com.onion.business.domain.user.authorities.AuthorityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Supplier;

@Slf4j
@Service
public class JpaUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final BCryptPasswordEncoder encoder;

    @Autowired
    public JpaUserDetailsService(UserRepository userRepository, AuthorityRepository authorityRepository, BCryptPasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.encoder = encoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Supplier<UsernameNotFoundException> s =
                () -> new UsernameNotFoundException(
                        "Problem during authentication!");

        User u = userRepository
                .findUserByUsername(username)
                .orElseThrow(s);

        return new CustomUserDetails(u);
    }

    @Transactional
    public User createUser(UserDto user) {
        userRepository.findUserByUsername(user.username()).ifPresent((u) -> {
            throw new RuntimeException("User Already Exists!");
        });
        Authority userAuthority = authorityRepository.findAuthorityByName("USER")
                .orElseThrow(RuntimeException::new);
        String password = encoder.encode(user.password());
        User u = User.builder()
                .username(user.username())
                .password(password)
                .authorities(List.of(userAuthority))
                .build();
        User savedUser = userRepository.save(u);
        log.info("User created!");
        return savedUser;
    }
}

