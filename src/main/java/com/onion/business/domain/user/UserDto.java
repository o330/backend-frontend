package com.onion.business.domain.user;

public record UserDto(String username, String password) {
}
