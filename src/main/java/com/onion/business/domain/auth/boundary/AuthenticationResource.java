package com.onion.business.domain.auth.boundary;

import com.onion.business.domain.auth.control.AuthenticationService;
import com.onion.business.domain.user.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.onion.business.domain.auth.AuthenticationConstants.AUTHENTICATION_URI;
import static com.onion.business.domain.auth.AuthenticationConstants.HEADER_STRING;
import static com.onion.business.domain.auth.AuthenticationConstants.TOKEN_PREFIX;


@RestController
@RequestMapping(AUTHENTICATION_URI)
@CrossOrigin
public class AuthenticationResource {
    private final AuthenticationService authenticationService;

    @Autowired
    public AuthenticationResource(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    //TODO przekazywac cały obiekt
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody UserDto user) {
        String jwt = authenticationService.authenticate(user);
        return ResponseEntity.status(HttpStatus.OK)
                .header(HEADER_STRING, TOKEN_PREFIX + jwt)
                .body(null);
    }

    @GetMapping("/auth")
    public void auth() {
    }
}
