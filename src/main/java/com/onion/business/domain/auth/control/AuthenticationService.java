package com.onion.business.domain.auth.control;

import com.onion.business.domain.user.JpaUserDetailsService;
import com.onion.business.domain.user.User;
import com.onion.business.domain.user.UserDto;
import com.onion.business.domain.user.UsernamePasswordAuthentication;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Service
public class AuthenticationService {
    private final AuthenticationManager authenticationManager;
    private final JpaUserDetailsService userService;
    @Value("${jwt.signing.key}")
    private String signingKey;

    public AuthenticationService(AuthenticationManager authenticationManager,
                                 JpaUserDetailsService userService) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
    }

    public String authenticate(UserDto user) {
        String username = user.username();
        String password = user.password();
        UsernamePasswordAuthentication usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthentication(username, password);
        authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        SecretKey key = Keys.hmacShaKeyFor(signingKey.getBytes(StandardCharsets.UTF_8));
        return Jwts.builder()
                .setClaims(Map.of("username", username))
                .signWith(key)
                .compact();
    }

    public User createUser(UserDto user) {
        return userService.createUser(user);
    }
}
