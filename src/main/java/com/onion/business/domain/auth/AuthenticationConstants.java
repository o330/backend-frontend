package com.onion.business.domain.auth;

import com.onion.business.domain.shared.Endpoints;

public class AuthenticationConstants {
    public static final String HEADER_STRING = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String AUTHENTICATION_URI = Endpoints.API_URI + "/auth";
}
