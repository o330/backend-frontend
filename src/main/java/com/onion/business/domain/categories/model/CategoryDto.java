package com.onion.business.domain.categories.model;

import java.util.Objects;

import static com.onion.business.domain.categories.control.CategoryService.getLocalizedCategory;

public record CategoryDto(Long id, String name, String colorScheme) {

    private static final String UNCATEGORIZED_COLOR_SCHEME = "dark";

    public static CategoryDto of(Category expenseCategory) {
        if (Objects.isNull(expenseCategory))
            return new CategoryDto(null, getLocalizedCategory(DefaultCategories.UNCATEGORIZED.name()), UNCATEGORIZED_COLOR_SCHEME);
        else return new CategoryDto(
                expenseCategory.getId(),
                expenseCategory.getName(),
                expenseCategory.getColorScheme());
    }
}
