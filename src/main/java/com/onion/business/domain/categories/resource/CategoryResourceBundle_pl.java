package com.onion.business.domain.categories.resource;

import com.onion.business.domain.categories.model.DefaultCategories;

import java.util.ListResourceBundle;

@SuppressWarnings("unused")
public class CategoryResourceBundle_pl extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        int length = DefaultCategories.values().length;
        Object[][] resource = new Object[length][2];

        resource[0][0] = DefaultCategories.HOME.name();
        resource[0][1] = "Dom";

        resource[1][0] = DefaultCategories.GROCERIES.name();
        resource[1][1] = "Zakupy spożywcze";

        resource[2][0] = DefaultCategories.BOOKS.name();
        resource[2][1] = "Książki";

        resource[3][0] = DefaultCategories.COMMUNICATION.name();
        resource[3][1] = "Transport";

        resource[4][0] = DefaultCategories.COMPANY.name();
        resource[4][1] = "Zakupy do firmy";

        resource[5][0] = DefaultCategories.EATING_OUT.name();
        resource[5][1] = "Jedzenie na mieście";

        resource[6][0] = DefaultCategories.EDUCATION.name();
        resource[6][1] = "Edukacja";

        resource[7][0] = DefaultCategories.ENTERTAINMENT.name();
        resource[7][1] = "Rozrywka";

        resource[8][0] = DefaultCategories.INVESTMENTS.name();
        resource[8][1] = "Inwestycje";

        resource[9][0] = DefaultCategories.RENT.name();
        resource[9][1] = "Czynsz";

        resource[10][0] = DefaultCategories.SELF_CARE.name();
        resource[10][1] = "Zdrowie";

        resource[11][0] = DefaultCategories.SUBSCRIPTIONS.name();
        resource[11][1] = "Subskrypcje";

        resource[12][0] = DefaultCategories.OTHER.name();
        resource[12][1] = "Inne";

        resource[13][0] = DefaultCategories.UNCATEGORIZED.name();
        resource[13][1] = "Bez kategorii";

        return resource;
    }
}
