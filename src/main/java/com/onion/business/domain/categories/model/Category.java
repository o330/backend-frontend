package com.onion.business.domain.categories.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@ToString
@Getter
@Setter
@Entity
public class Category {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String colorScheme;
    @Column(nullable = false)
    private Long userId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return getId().equals(category.getId()) && getName().equals(category.getName())
                && getColorScheme().equals(category.getColorScheme()) && getUserId().equals(category.getUserId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getColorScheme(), getUserId());
    }
}
