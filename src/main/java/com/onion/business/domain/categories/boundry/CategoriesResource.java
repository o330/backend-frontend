package com.onion.business.domain.categories.boundry;

import com.onion.business.domain.categories.control.CategoryService;
import com.onion.business.domain.categories.model.Category;
import com.onion.business.domain.categories.model.CategoryDto;
import com.onion.business.domain.user.UsernamePasswordAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/categories/")
public class CategoriesResource {

    private final CategoryService categoryService;

    @Autowired
    public CategoriesResource(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public List<CategoryDto> getCategories(UsernamePasswordAuthentication principal) {
        return categoryService.findCategoriesByUser(principal);
    }

    @PostMapping
    public List<Category> createExpenseCategory(UsernamePasswordAuthentication principal,
                                                @RequestBody List<Category> categories) {
        return categoryService.createCategory(categories, principal);
    }

    @PutMapping
    public List<Category> updateCategories(UsernamePasswordAuthentication principal,
                                           @RequestBody List<Category> categories) {
        return categoryService.updateCategories(categories, principal);
    }

    @GetMapping("/defaults")
    public List<String> getDefaultCategories() {
        return categoryService.getDefaults();
    }
}
