package com.onion.business.domain.categories.model;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public enum DefaultCategories {
    HOME("Home"),
    GROCERIES("Groceries"),
    ENTERTAINMENT("Entertainment"),
    BOOKS("Books"),
    EATING_OUT("Eating Out"),
    SUBSCRIPTIONS("Subscriptions"),
    RENT("Rent"),
    COMPANY("Company"),
    SELF_CARE("Self Care"),
    COMMUNICATION("Communication"),
    EDUCATION("Education"),
    INVESTMENTS("Investments"),
    OTHER("Other"),
    UNCATEGORIZED("Uncategorized");

    private final String displayName;

    DefaultCategories(String displayName) {
        this.displayName = displayName;
    }
}
