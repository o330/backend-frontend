package com.onion.business.domain.categories.control;

import com.onion.business.domain.categories.model.Category;
import com.onion.business.domain.categories.model.CategoryDto;
import com.onion.business.domain.categories.model.DefaultCategories;
import com.onion.business.domain.categories.resource.CategoryResourceBundle;
import com.onion.business.domain.user.User;
import com.onion.business.domain.user.UserRepository;
import com.onion.business.domain.user.UsernamePasswordAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Service
public class CategoryService {
    public static final String CATEGORY_RESOURCE_BUNDLE = CategoryResourceBundle.class.getName();
    private final CategoryRepository categoryRepository;
    private final UserRepository userRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository,
                           UserRepository userRepository) {
        this.categoryRepository = categoryRepository;
        this.userRepository = userRepository;
    }

    public static String getLocalizedCategory(String category) {
        ResourceBundle categoryBundle = ResourceBundle
                .getBundle(CATEGORY_RESOURCE_BUNDLE, LocaleContextHolder.getLocale());
        return categoryBundle.getString(category);
    }

    public List<CategoryDto> findCategoriesByUser(UsernamePasswordAuthentication principal) {
        return categoryRepository
                .findByUserId(principal.getUser().getId())
                .stream()
                .map(CategoryDto::of)
                .toList();
    }

    public List<Category> createCategory(List<Category> categories,
                                         UsernamePasswordAuthentication principal) {
        Long id = principal.getUser().getId();
        categories.forEach(category -> category.setUserId(id));
        return categoryRepository.saveAll(categories);
    }

    @Transactional
    public List<Category> updateCategories(List<Category> categories,
                                           UsernamePasswordAuthentication principal) {
        User user = principal.getUser();
        categories.forEach(c -> c.setUserId(user.getId()));
        user.setExpenseCategories(categories);
        return userRepository.save(user).getExpenseCategories();
    }

    public List<String> getDefaults() {
        ResourceBundle categoryBundle = ResourceBundle
                .getBundle(CATEGORY_RESOURCE_BUNDLE, LocaleContextHolder.getLocale());
        return categoryBundle
                .keySet()
                .stream()
                .filter(key -> !key.equals(DefaultCategories.UNCATEGORIZED.name()))
                .map(categoryBundle::getString)
                .sorted()
                .collect(Collectors.toList());
    }
}
