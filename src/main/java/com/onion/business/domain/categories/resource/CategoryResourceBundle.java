package com.onion.business.domain.categories.resource;

import com.onion.business.domain.categories.model.DefaultCategories;

import java.util.ListResourceBundle;

@SuppressWarnings("unused")
public class CategoryResourceBundle extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        DefaultCategories[] values = DefaultCategories.values();
        int length = values.length;
        Object[][] resource = new Object[length][2];
        for (int i = 0; i < values.length; i++) {
            resource[i][0] = values[i].name();
            resource[i][1] = values[i].getDisplayName();
        }
        return resource;
    }
}
