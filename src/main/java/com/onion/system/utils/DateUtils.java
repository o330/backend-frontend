package com.onion.system.utils;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.Locale;

public class DateUtils {
    public static LocalDateTime atStartOfDay(LocalDateTime date) {
        return date.with(LocalTime.MIN);
    }

    public static LocalDateTime startOfToday() {
        return atStartOfDay(LocalDateTime.now());
    }

    public static LocalDateTime atEndOfDay(LocalDateTime date) {
        return date.with(LocalTime.MAX);
    }

    public static LocalDateTime endOfToday() {
        return atEndOfDay(LocalDateTime.now());
    }

    public static LocalDateTime firstDayOfCurrentMonth() {
        LocalDateTime today = LocalDateTime.now();
        LocalDateTime firstDayOfMonth = today.withDayOfMonth(1);
        return firstDayOfMonth.with(LocalTime.MIN);
    }

    public static LocalDateTime firstDayOfCurrentWeek() {
        LocalDateTime today = LocalDateTime.now();
        TemporalField fieldISO = WeekFields.of(Locale.FRANCE).dayOfWeek();
        LocalDateTime firstDayOfWeek = today.with(fieldISO, 1);
        return firstDayOfWeek.with(LocalTime.MIN);
    }
}
